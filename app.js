Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'TaskList',

    appFolder: 'app',
    controllers: ['Tasks'],
    views: [
        'task.List',
        'task.Card',
        'changeLog.List',
        'changeLog.Card',
        'chart.PieChartWindow',
        'chart.LineChartWindow',
        'category.AddWindow'
    ],

    launch: function() {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [
                {
                    xtype: 'tasklist'
                }
            ]
        });
    }
});