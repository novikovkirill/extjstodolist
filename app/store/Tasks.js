Ext.define('TaskList.store.Tasks', {
    extend: 'Ext.data.Store',
    model: 'TaskList.model.Task',

    autoLoad: true
});