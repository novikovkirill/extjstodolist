Ext.define('TaskList.store.ChartTypes', {
    extend: 'Ext.data.Store',
    fields: ['name', 'code'],

    data: [
    	{name: 'Pie chart of current tasks', code: 'PieChart'},
    	{name: 'Change log visualization', code: 'LineChart'}
    ]
});