Ext.define('TaskList.store.FieldsForPieChart', {
    extend: 'Ext.data.Store',
    fields: ['name', 'value'],

    data: [
    	{name: 'Status', value: 'status'},
    	{name: 'Label', value: 'label'},
    	{name: 'Category', value: 'category'}
    ]
});