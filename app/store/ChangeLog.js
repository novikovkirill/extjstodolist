Ext.define('TaskList.store.ChangeLog', {
    extend: 'Ext.data.Store',
    model: 'TaskList.model.TaskChangeEvent',

    autoLoad: true,

    makeEventLog: function(eventType, oldValues, newValues, addedId){
    	switch (eventType){
    		case 'add':
		        for (let key in newValues) {
		        	if (newValues[key]){
		                this.add({
		                    event: 'add',
		                    taskId: addedId,
		                    taskName: newValues.name,
		                    changedField: key,
		                    previousValue: '',
		                    nextValue: newValues[key],
		                    time: Ext.Date.format(new Date(), 'd.m.Y H:i:s')
		                });
		            }
		        }
		        break;
		    case 'update':
		        for (let key in newValues) {
		            if (oldValues[key] !== newValues[key]){
		                this.add({
		                    event: 'update',
		                    taskId: oldValues.id,
		                    taskName: oldValues.name,
		                    changedField: key,
		                    previousValue: oldValues[key],
		                    nextValue: newValues[key],
		                    time: Ext.Date.format(new Date(), 'd.m.Y H:i:s')
		                });
		            }
		        }
		        break;
		    case 'delete': 
                this.add({
                    event: 'delete',
                    taskId: oldValues.id,
                    taskName: oldValues.name,
                    changedField: 'status',
                    previousValue: oldValues.status,
                    nextValue: '',
                    time: Ext.Date.format(new Date(), 'd.m.Y H:i:s')
                });
		        break;
		}
    } 

});