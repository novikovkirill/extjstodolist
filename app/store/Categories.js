Ext.define('TaskList.store.Categories', {
    extend: 'Ext.data.Store',
    model: 'TaskList.model.Category',

    autoLoad: true
});