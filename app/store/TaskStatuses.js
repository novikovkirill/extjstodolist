Ext.define('TaskList.store.TaskStatuses', {
    extend: 'Ext.data.Store',
    model: 'TaskList.model.TaskStatus',

    data: [
    	{name: 'Planned'},
    	{name: 'In progress'},
    	{name: 'Done'},
    	{name: 'Expired'}
    ]
});