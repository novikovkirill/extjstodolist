Ext.define('TaskList.controller.Tasks', {
    extend: 'Ext.app.Controller',

    stores: [
        'Categories',
        'Tasks',
        'TaskStatuses',
        'ChangeLog',
        'ChartTypes',
        'FieldsForPieChart'
    ],
    models: [
        'Category',
        'Task',
        'TaskStatus',
        'TaskChangeEvent'
    ],

    refs: [
        {
            ref: 'taskCardForm',
            selector: 'taskcard form'
        },
        {
            ref: 'taskCard',
            selector: 'taskcard'
        },
        {
            ref: 'taskList',
            selector: 'tasklist'
        },
        {
            ref: 'deleteButton',
            selector: 'tasklist button[action=delete]'
        },
        {
            ref: 'statusCombo',
            selector: 'tasklist combo'
        },
        {
            ref: 'viewChangeLogButton',
            selector: 'tasklist button[action=viewChangeLog]'
        },
        {
            ref: 'categoryAddWindow',
            selector: 'categoryaddwindow'
        },
        {
            ref: 'addCategoryForm',
            selector: 'categoryaddwindow form'
        }
    ],

    init: function() {
        this.control({
            'tasklist': {
                selectionchange: this.onTaskListSelectionChange,
                itemdblclick: this.onRowDblClick
            },
            'tasklist button[action=add]': {
                click: this.openAddForm
            },
            'tasklist combo#statusChangeCombo':{
                select: this.onStatusComboSelect
            },
            'tasklist combo#chartCombo':{
                select: this.onChartComboSelect
            },
            'tasklist button[action=delete]': {
                click: this.deleteTasks
            },  
            'tasklist button[action=viewChangeLogCommon]': {
                click: this.openCommonChangeLog
            },
            'tasklist button[action=viewChangeLogSelected]':{
                click: this.openSelectedChangeLog
            },
            'taskcard button[action=add]': {
                click: this.addTask
            },
            'taskcard button[action=update]': {
                click: this.updateTask
            },
            'taskcard button[action=viewChangeLog]': {
                click: this.openSelectedChangeLog
            },
            'taskcard button[action=addCategory]': {
                click: this.openAddCategoryForm
            },
            'categoryaddwindow button[action=add]': {
                click: this.addCategory
            }
        });
    },

    openAddForm: function(){
        Ext.widget('taskcard', {mode: 'add'});
    },

    addTask: function(){
        const form = this.getTaskCardForm(),
            tasksStore = this.getStore('Tasks'),
            changeLog = this.getStore('ChangeLog');
        if (form.isValid()){
            const formValues = form.getValues();
            tasksStore.add(formValues);
            tasksStore.sync();
            const addedId = tasksStore.last().get('id');
            changeLog.makeEventLog('add', {}, formValues, addedId);
            changeLog.sync();
            this.getTaskCard().close();
            Ext.Msg.alert('', 'Task succesfully added');
        }
    },

    deleteTasks: function(){
        Ext.Msg.confirm('', 'Delete selected tasks?', (answer) =>
            {
                if (answer === 'yes'){
                    const selectedRecords = this.getTaskList().getSelectionModel().getSelection(),
                        tasksStore = this.getStore('Tasks'),
                        changeLog = this.getStore('ChangeLog');
                    tasksStore.remove(selectedRecords);
                    Ext.each(selectedRecords, (selectedRecord) => {
                        changeLog.makeEventLog('delete',selectedRecord.data);
                    });
                    tasksStore.sync();
                    changeLog.sync();
                }
            }
        )
    },

    onTaskListSelectionChange: function(){
        const taskList = this.getTaskList(),
            topToolBar = taskList.getDockedItems('toolbar[dock="top"]')[0];
            toggleButtons = topToolBar.query('[cls=toggle]'),
            selectedCount = taskList.getSelectionModel().getCount();
        if (selectedCount > 0){
            Ext.each(toggleButtons, (tBarEl) => {
                let isDisabled = !(selectedCount === 1 || tBarEl.getItemId() !== 'viewChangeLogSelected');
                tBarEl.setDisabled(isDisabled);
            });
        } else {
            Ext.each(toggleButtons, (tBarEl) => {
                tBarEl.setDisabled(true);
            });
        }
    },

    onStatusComboSelect: function(combo){
        Ext.Msg.confirm('', 'Change selected records status?', (answer) =>
            {
                const selectedRecords = this.getTaskList().getSelectionModel().getSelection(),
                    tasksStore = this.getStore('Tasks'),
                    changeLog = this.getStore('ChangeLog');
                Ext.each(selectedRecords, (record) => {
                    const copy = record.copy();
                    record.set('status',combo.getValue());
                    changeLog.makeEventLog('update', copy.data, record.data);
                });
                changeLog.sync();
                combo.clearValue();
                tasksStore.sync();
            }
        )
    },

    onRowDblClick: function(grid, record){
        const card = Ext.widget('taskcard',{mode: 'update'});
        card.down('form').loadRecord(record);
        card.setUpdateMode(record);
    },

    updateTask: function(){
        const taskList = this.getTaskList(),
            selectedRecord = taskList.getSelectionModel().getSelection()[0],
            form = this.getTaskCardForm(),
            tasksStore = this.getStore('Tasks'),
            changeLog = this.getStore('ChangeLog');
        if (form.isValid()){
            const formValues = form.getValues();
            changeLog.makeEventLog('update', selectedRecord.data, formValues);
            for (let key in formValues) {
                if (selectedRecord.get(key) !== formValues[key]){
                    selectedRecord.set(key, formValues[key]);
                }
            }
            tasksStore.sync();
            changeLog.sync();
            Ext.Msg.alert('', 'Task succesfully updated');
        }
    },

    openCommonChangeLog: function(){
        this.getStore('ChangeLog').clearFilter();
        Ext.widget('changelogcard');
    },

    openSelectedChangeLog: function(){
        const selectedRecord = this.getTaskList().getSelectionModel().getSelection()[0];
        this.getStore('ChangeLog').clearFilter();
        this.getStore('ChangeLog').filter('taskId', selectedRecord.get('id'));
        Ext.widget('changelogcard');
    },

    onChartComboSelect: function(combo, records){
        const tasksStore = this.getStore('Tasks'),
            changeLog = this.getStore('ChangeLog');
        switch (records[0].get('code')){
            case 'PieChart':
                if (tasksStore.getCount() > 0){
                    Ext.widget('piechartwindow', {store: this.getStore('Tasks')});
                } else {
                    Ext.Msg.alert('', 'Task list is empty, nothing to visualize');
                }
                break;
            case 'LineChart':
                if (changeLog.getCount() > 0){
                    Ext.widget('linechartwindow', {store: this.getStore('ChangeLog')});
                } else {
                    Ext.Msg.alert('', 'Change log is empty, nothing to visualize');
                }           
                break;                
        }
        combo.clearValue();
    },

    addCategory: function(){
        const form = this.getAddCategoryForm(),
            categoriesStore = this.getStore('Categories');
        if (form.isValid()){
            const formValues = form.getValues();
            categoriesStore.add(formValues);
            categoriesStore.sync();
            this.getCategoryAddWindow().close();
            Ext.Msg.alert('', 'Category succesfully added');
        }
    },

    openAddCategoryForm: function(){
        Ext.widget('categoryaddwindow');
    }

});