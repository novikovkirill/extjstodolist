Ext.define('TaskList.view.category.AddWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.categoryaddwindow',

    title: 'Add category',
    layout: 'fit',
    autoShow: true,
    modal: true,

    constructor: function() {

        this.items = {
            xtype: 'form',
            items: {
                xtype: 'fieldset',
                border: false,
                margin: '5 0 0 0',
                items: {
                    xtype: 'textfield',
                    fieldLabel: 'Category name',
                    name: 'name',
                    allowBlank: false
                }
            }
        };

        this.buttons = [{
            text: 'Add',
            action: 'add'
        }];

        this.callParent(arguments);
    }

});