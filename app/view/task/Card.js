Ext.define('TaskList.view.task.Card', {
    extend: 'Ext.window.Window',
    alias: 'widget.taskcard',

    title: 'New task',
    layout: 'fit',
    autoShow: true,
    width: 700,
    modal: true,

    constructor: function(config) {

        this.items = {
            xtype: 'form',
            defaults: {
                xtype: 'fieldset',
                defaults: {
                    allowBlank: false
                },
                border: false,
                margin: '5 0 5 0'
            },
            items: [
                {   
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'name',
                            fieldLabel: 'Task name',
                            width: 350,
                        },
                        {xtype: 'tbfill'},
                        {
                            xtype: 'button',
                            action: config.mode === 'update' ? 'viewChangeLog' : '',
                            hidden: config.mode === 'add',
                            text: 'View ChangeLog',
                        }
                    ]
                },
                {
                    layout: 'fit',
                    items: {
                        xtype: 'textarea',
                        name: 'description',
                        fieldLabel: 'Task description',
                        allowBlank: true
                    }
                },
                {
                    items: {
                        xtype: 'datefield',
                        name: 'dueDate',
                        fieldLabel: 'Due date',
                        format: 'd.m.Y',
                        minValue: new Date(),
                        startDay: 1
                    }
                },
                {
                    items: {
                        xtype: 'combo',
                        name: 'status',
                        fieldLabel: 'Status',
                        displayField: 'name',
                        readOnly: config.mode === 'add',
                        queryMode: 'local',
                        store: 'TaskStatuses',
                        value: 'Planned'
                    }
                },
                {
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'combo',
                            name: 'category',
                            fieldLabel: 'Category',
                            allowBlank: true,
                            displayField: 'name',
                            inputAttrTpl: " data-qtip='Type category or select from existing' ",
                            queryMode: 'local',
                            store: 'Categories'
                        },
                        {
                            xtype: 'button',
                            text: 'Add category',
                            action: 'addCategory',
                            margin: '0 0 0 15'
                        }   
                    ]                
                },
                {
                    items: {
                        xtype: 'textfield',
                        name: 'label',
                        fieldLabel: 'Label',
                        allowBlank: true
                    }
                }
            ]
        };

        this.buttons = [
            {text: 'Save', action: config.mode || 'add'},
            {text: 'Cancel', scope: this, handler: this.close}
        ];

        this.callParent(arguments);
    },

    setUpdateMode: function(record){
        this.setTitle(record.get('name'));
    }
});