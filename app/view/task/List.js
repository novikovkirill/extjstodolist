Ext.define('TaskList.view.task.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.tasklist',

    store: 'Tasks',
    selType: 'checkboxmodel',
    viewConfig: {
        getRowClass: function(record){
            return `row-${record.get('status').split(' ').join('').toLowerCase()}`
        }
    },

    initComponent: function() {

        this.columns = [
            {header: 'Task',  dataIndex: 'name',  flex: 2},
            {header: 'Due date', dataIndex: 'dueDate', flex: 1},
            {header: 'Status', dataIndex: 'status', flex: 1},
            {header: 'Category', dataIndex: 'category', flex: 1},
            {header: 'Label', dataIndex: 'label', flex: 1},
        ];

        this.tbar = [
            {xtype: 'button', action: 'add', text: 'Add task'},
            {xtype: 'button', action: 'delete', disabled: true, text: 'Delete tasks', cls: 'toggle'},
            {
                xtype: 'combo',
                fieldLabel: 'Mark selected tasks as', 
                itemId: 'statusChangeCombo',
                displayField: 'name',
                editable: false,
                disabled: true,
                labelWidth: 150,
                queryMode: 'local',
                store: 'TaskStatuses',
                valueField: 'name',
                cls: 'toggle',
                width: 250
            },
            {
                xtype: 'button', 
                action: 'viewChangeLogCommon', 
                text: 'View Change Log for all tasks',
            },
            {
                xtype: 'button', 
                action: 'viewChangeLogSelected', 
                disabled: true, 
                text: 'View Change Log for selected task', 
                itemId: 'viewChangeLogSelected',
                cls: 'toggle'
            },
            {
                xtype: 'combo',
                fieldLabel: 'View chart',
                displayField: 'name',
                editable: false,
                itemId: 'chartCombo',
                labelWidth: 80,
                queryMode: 'local',
                store: 'ChartTypes',
                valueField: 'name',
                width: 280
            }
        ];

        this.callParent(arguments);
    }
});