Ext.define('TaskList.view.changeLog.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.changeloglist',

    store: 'ChangeLog',

    initComponent: function() {

        this.columns = [
            {header: 'Task Name', dataIndex: 'taskName', flex: 1.5},
            {header: 'Event', dataIndex: 'event', flex: 1},
            {header: 'Changed field', dataIndex: 'changedField', flex: 1.1},
            {
                header: 'Previous value',
                dataIndex: 'previousValue',
                flex: 2,
                renderer: this.fieldValueRenderer
            },
            {
                header: 'Next Value', 
                dataIndex: 'nextValue', 
                flex: 2,
                renderer: this.fieldValueRenderer
            },
            {header: 'Time', dataIndex: 'time', flex: 1.5}
        ];

        this.callParent(arguments);
    },

    fieldValueRenderer: function(value, metaData){
        metaData.tdAttr = 'data-qtip="' + value + '"';
        return value;
    }
});