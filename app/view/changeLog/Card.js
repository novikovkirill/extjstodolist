Ext.define('TaskList.view.changeLog.Card', {
    extend: 'Ext.window.Window',
    alias: 'widget.changelogcard',

    title: 'Change log',
    layout: 'fit',
    autoShow: true,
    modal: true,
    width: 900,
    height: 400,

    constructor: function() {

        this.items = {
            xtype: 'changeloglist',
        };

        this.callParent(arguments);
    }

});