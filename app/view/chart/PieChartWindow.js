Ext.define('TaskList.view.chart.PieChartWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.piechartwindow',

    title: 'Chart',
    layout: 'fit',
    autoShow: true,
    width: 700,
    height: 500,
    resizable: false,

    constructor: function(config) {

		const firstField = Ext.create('TaskList.store.FieldsForPieChart').first().get('value'),
			tasksStore = config.store,
			chart = this.makeChartForField(tasksStore,firstField);

		this.tasksStore = tasksStore;
	    this.items = chart;

	    this.tbar = [
	    	{xtype: 'tbfill'},
		    {
	    		xtype: 'combo',
	    		fieldLabel: 'Make chart for field',
	    		displayField: 'name',
	    		editable: false,
	    		labelWidth: 120,
	    		queryMode: 'local',
	    		listeners: {
	    			select: this.onChartFieldComboSelect.bind(this)
	    		},
	    		store: 'FieldsForPieChart',
	    		value: Ext.create('TaskList.store.FieldsForPieChart').first(),
	    		valueField: 'value'
		    }
	    ];

        this.callParent(arguments);
    },

    onChartFieldComboSelect: function(combo){
    	this.removeAll();
    	this.add(this.makeChartForField(this.tasksStore, combo.getValue()));
    },

    makeChartForField: function(store, field){
    	const uniqueValues = store.collect(field, true),
    		chartRecords = uniqueValues.map((value)=>{
    			return {
					name: value || `No ${field}`,
					count: 0
	    		}
    	});

    	store.each((task) => {
    		chartRecords.find((chartRecord) => chartRecord.name === (task.get(field) || `No ${field}`)).count++;
    	});

    	const chartStore = Ext.create('Ext.data.Store', {
    		fields: ['name', 'count'],
    		data: chartRecords
    	});

    	return Ext.create('Ext.chart.Chart', {
	        xtype: 'chart',
	        animate: true,
	        store: chartStore,
	        shadow: true,
	        legend: {
	            position: 'right'
	        },
	        insetPadding: 60,
	        theme: 'Base:gradients',
	        series: [{
	            type: 'pie',
	            field: 'count',
	            showInLegend: true,
	            donut: false,
	            tips: {
	                trackMouse: true,
	                width: 120,
	                renderer: function(storeItem, item) {
	                    this.setTitle(storeItem.get('name') + ': ' + Math.round(storeItem.get('count') / store.getCount() * 100) + '%');
	                }
	            },
	            highlight: {
	                segment: {
	                    margin: 20
	                }
	            },
	            label: {
	                field: 'name',
	                display: 'rotate',
	                contrast: true,
	                font: '18px Arial'
	            }
	        }]
	    });
    }

});