Ext.define('TaskList.view.chart.LineChartWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.linechartwindow',

    title: 'Chart',
    layout: 'fit',
    autoShow: true,
    width: 700,
    height: 500,
    resizable: false,

    constructor: function(config) {

		const changeLog = config.store,
			chart = this.makeChart(changeLog);

	    this.items = chart;

        this.callParent(arguments);
    },

    makeChart: function(changeLog){

    	const statusesStore = Ext.create('TaskList.store.TaskStatuses');

    	return Ext.create('Ext.chart.Chart', {
	        style: 'background:#fff',
	        animate: true,
	        store: this.makeChartStore(changeLog),
	        shadow: true,
	        legend: {
	            position: 'right'
	        },
	        axes: [{
	            type: 'Numeric',
	            minimum: 0,
	            position: 'left',
	            fields: ['Total'].concat(statusesStore.collect('name')),
	            title: 'Number of tasks'
	        }, {
	            type: 'Category',
	            position: 'bottom',
	            fields: ['date'],
	            title: 'Date'
	        }],
	        series: this.makeChartSeries()
	    });
    },

    makeChartStore: function(changeLog){
    	const statusesStore = Ext.create('TaskList.store.TaskStatuses');
    	let currentDate = '',
    		dateRecord = this.initDateRecordObject(),
    		dateRecords = [];
    	changeLog.filter('changedField','status');
    	changeLog.sort('date');
    	changeLog.each((record) => {
    		if (record.get('date') !== currentDate){
    			if (currentDate !== ''){
    				dateRecords.push(
    					Ext.apply(
    						dateRecord,
    						{date: currentDate}
    					)
    				);
    				dateRecord = this.initDateRecordObject(dateRecord);
    			}
    			currentDate = record.get('date');
    		}
    		switch (record.get('event')){
    			case 'add':
    				dateRecord.Planned++;
    				dateRecord.Total++;
    				break;
    			case 'update':
    				dateRecord[record.get('previousValue')]--;
    				dateRecord[record.get('nextValue')]++;
    				break;
    			case 'delete':
    			    dateRecord[record.get('previousValue')]--;
    				dateRecord.Total--;
    				break;
    		}
    	});
		dateRecords.push(
			Ext.apply(
				dateRecord,
				{date: currentDate}
			)
		);

    	return Ext.create('Ext.data.Store',{
    		fields: ['date', 'Total'].concat(statusesStore.collect('name')),
    		data: dateRecords
    	})
    },

    initDateRecordObject: function(prevObject){
    	if (!prevObject){
	    	const statusesStore = Ext.create('TaskList.store.TaskStatuses');
	    	let dateRecord = {Total: 0 };
	    	statusesStore.each((record) => {
	    		dateRecord[record.get('name')] = 0;
	    	});
	    	return dateRecord;
	    } else {
	    	let dateRecord = {};
	    	for (let key in prevObject){
	    		dateRecord[key] = prevObject[key];
	    	}
	    	return dateRecord;
	    }
    },

    makeChartSeries: function(){
    	const statusesStore = Ext.create('TaskList.store.TaskStatuses');
    	return ['Total'].concat(statusesStore.collect('name')).map((status) => {
    			return {
		            type: 'line',
		            highlight: {
		                size: 7,
		                radius: 7
		            },
		            axis: 'left',
		            xField: 'date',
		            yField: status,
		            markerConfig: {
		                type: 'circle',
		                size: 4,
		                radius: 4,
		       		}
		    	}
		    }
    	)
    }

});