Ext.define('TaskList.model.TaskStatus', {
    extend: 'Ext.data.Model',
    fields: ['name']
});