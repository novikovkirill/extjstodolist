Ext.define('TaskList.model.TaskChangeEvent', {
    extend: 'Ext.data.Model',
    fields: [
        'event',
        'taskId',
        'taskName',
        'changedField',
        'previousValue',
        'nextValue',
        'time',
        {
            name: 'date',
            convert: function(v, record) {
                return record.get('time').split(' ')[0];
            }
        }
    ],
    proxy: {
        type: 'localstorage',
        id  : 'tasksjournal'
    }
});