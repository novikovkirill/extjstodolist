Ext.define('TaskList.model.Category', {

    extend: 'Ext.data.Model',
    fields: ['name'],
    proxy: {
        type: 'localstorage',
        id  : 'categories'
    }

});