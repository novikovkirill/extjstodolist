Ext.define('TaskList.model.Task', {
    extend: 'Ext.data.Model',
    fields: [
    	'name',
        'category',
    	'description', 
    	{name: 'dueDate', mapping: 'due_date'},
    	'status',
    	'label'
    ],
    proxy: {
        type: 'localstorage',
        id  : 'tasks'
    }
});